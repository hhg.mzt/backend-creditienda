# backend-creditienda
Este proyecto fue construido con python 3.8 
## Antes de comenzar ##
1. Clonar el reporsitorio

` git clone https://github.com/HiramHernandez/bioteksa.git`

2. Crear el entorno virtual

 `virtualenv RUTA_DEL_ENTORNO_VIRTUAL/NOMBRE_ENTORNO`

3. Activar el entorno 
	1. En windows
		`RUTA_DEL_ENTORNO_VIRTUAL/Scripts/activate.bat`
4. Instalar las dependencias en raíz del proyecto:

	`pip install -r requirements.txt`

## Configuración ##

5. Crear archivo settings.json, en raiz del proyecto.
	```json
	{ 	"DEBUG": "",
		"SQLALCHEMY_TRACK_MODIFICATIONS": "",
		"SQLALCHEMY_DATABASE_URI": "" 
	}
	```

La tercera llave sera el string de conexión formateado para reconocer caracteres especiales.
Para más información acerca de la configuración de SQLAlchemy visitar:
http://flask-sqlalchemy.pocoo.org/2.3/config/.

## Ejecutar la aplicación ##
En raiz del proyecto

	`python app,py`
