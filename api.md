## Listado de Endpoints api ##
- Prefijo **/api**
- ENDPOITS
- GET, POST ***/prospect***, Lista de todos los prospectos e inserción de un prospecto
	- RESPUESTA (GET)
   

----------
 
    		{
	    		"data":[
	    
	    	 		{
	    				"id_prospecto": 5,
	    				"nombre": "Irving",
	    				"primer_apellido": "Sotelo",
	    				"segundo_apellido": "",
	    				"telefono": "000000",
	    				"rfc": "000000",
	    				"status": "Autorizado",
	    				"observaciones": ""
	    			}
	    		....
	    		]
    		}

	- BODY (POST)

----------

	    	{
    		"nombre": "Hiram",
    		"primer_apellido": "Hernandez",
    		"segundo_apellido": "Garcia",
    		"calle": "MERCURIO",
    		"numero_casa": "4001",
    		"colonia": "RAMON F ITURBE",
    		"codigo_postal": 82127,
    		"telefono": "6691983947",
    		"rfc": "0000000011111",
    		"id_estado": 1
    		}


- GET, PUT ***/prospect/:idProspecto***, del parametro de uri trae la información del prospecto que corresponde a ese ID en caso de encontrarse y en PUT actualiza el estado de ese prospecto en caso de encontrarse (ENVIADO, AUTORIZADO o RECHAZADO)
	
	- Response (GET)

----------

		 {
		    "data": {
		        "id_prospecto": 2,
		        "nombre": "RODOLFO",
		        "primer_apellido": "HERNANDEZ",
		        "segundo_apellido": "",
		        "telefono": "669383838",
		        "rfc": "9949494",
		        "status": "Autorizado",
		        "observaciones": ""
		    },
    		"message": "success"
		}


----------

- BODY (PUT)



     {
     	"autorizar": 0,
     	"observaciones": "No cuenta con aval"
     }
