import os
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from src.models import Prospect
from src.urls import urls
from src.extensions import db

app = Flask(__name__)
CORS(app)
#cors = CORS(app, resources={r"/foo": {"origins": "*"}})
#app.config['CORS_HEADERS'] = 'Content-Type'
app.config.from_json(os.path.abspath(os.path.join('settings.json')))
db.init_app(app)
app.register_blueprint(urls)

@app.route("/")
def index():
    #prospects = db.session.query(Prospect.nombre).all()
    prospects = Prospect.query.all()
    for prospect in prospects:
        str_prospect = f"{prospect.nombre} - {prospect.primer_apellido} - {prospect.segundo_apellido}"
        print(str_prospect)
    return "<H1>HOLA MUNDO </H2>"



@app.route('/help', methods=['GET'])
def routes_info():
    """Print all defined routes
    This also handles flask-router, which uses a centralized scheme
    to deal with routes, instead of defining them as a decorator
    on the target function.
    """
    routes = []
    for rule in app.url_map.iter_rules():
        try:
            if rule.endpoint != 'static':

                if hasattr(app.view_functions[rule.endpoint], 'import_name'):
                    import_name = app.view_functions[rule.endpoint].import_name
                    obj = import_string(import_name)
                    routes.append({rule.rule: "%s\n%s" % (",".join(list(rule.methods)), obj.__doc__), 'methods': list(rule.methods)})
                else:
                    body = dict()
                    # to verify view_class attribute we ensure that endpoint was created through from 
                    # object api restful
                    if 'view_class' in dir(app.view_functions[rule.endpoint]):
                        view_resource = app.view_functions[rule.endpoint].view_class
                        if 'parser' in dir(view_resource):
                            for arg in view_resource.parser.args:
                                body.update({arg.name: 'type: {0}, required: {1}'.format(arg.type.__name__, arg.required)})
                    routes.append({'endpoint': rule.rule, 'methods': list(rule.methods), 'body': body})

        except Exception as exc:

            routes.append({rule.rule: "(%s) INVALID ROUTE DEFINITION!!!" % rule.endpoint})
            route_info = "%s => %s" % (rule.rule, rule.endpoint)
            app.logger.error("Invalid route: %s" % route_info, exc_info=True)

    return jsonify(code=200, data=routes)


def sumar(a: int, b: int) -> int:
    return a + b

resultado = sumar(25, 25)
print(resultado)
if __name__ == '__main__':
    app.run()




