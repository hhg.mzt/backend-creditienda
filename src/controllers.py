import werkzeug
from flask_restful import Resource, request, reqparse
from src.services import ProspectService



class ProspectListController(Resource):
    def __init__(self):
        self.prospect_service = ProspectService()

    def post(self):
        payload = request.get_json(force=True)
        new_prospect =  self.prospect_service.creat_prospect(**payload)
        if not new_prospect[0]:
            return {'data': None, 'message': 'Ocurrio un error'}, 400
        return {'data': new_prospect[1]}, 201

    def get(self):
        prospects = self.prospect_service.fetch()
        if not prospects:
            return {'message': 'No se encontraron prospectos'}, 404
        return {'data': prospects, 'message': 'success'}, 200

class ProspectController(Resource):
    def __init__(self):
        self.prospect_service = ProspectService()

    def get(self, id_prospect):
        prospect = self.prospect_service.retrieve(id_prospect)
        if not prospect:
            return {'data': None, 'message': 'No se encontró el prospecto'}, 404
        return {'data': prospect, 'message': 'success'}, 200

    def put(self, id_prospect):
        payload = request.get_json(force=True)
        print(payload)
        if int(payload['autorizar']) > 1:
            return {'message': 'Error estas enviando un valor invalidó'}, 400
        response = self.prospect_service.set_status(id_prospect, **payload)
        if not response:
            return {'message': 'Ocurrio un erro no se aplico la asignación'}, 400
        return {'message': 'Se aplico la asignación'}, 200


class DocumentListControllers(Resource):
    def __init__(self):
        self.prospect_service = ProspectService() 
    
    def post(self):
        payload = request.get_json(force=True)
        new_document =  self.prospect_service.create_document(**payload)
        if not new_document:
            return {'message': 'error al crear el documento'}, 400
        return {'message': 'success'}, 201




class AttachmentFileController(Resource):
    def __init__(self):
        self.storage_service = StorageBody()
        self.purcha_invoice_service = PurchaseInvoiceService()
        

    def post(self, pk):
        parser = reqparse.RequestParser()
        parser.add_argument('pdf', type=werkzeug.datastructures.FileStorage, location='files')
        body = self.parser.parse_args()
        pdf = body['pdf']
    
        if not pdf or not xml:
            return {'data': [], 'message': 'No se ha adjuntado los dos archivos'}, 400

        msg_not_allowed = 'El archivo file_name no tiene extensión válida archivos permitidos xml y pdf'
        if not allowed_file(xml.filename):
            return {'data': [], 'message': msg_not_allowed.replace('file_name', xml.filename)}, 400
        if not allowed_file(pdf.filename):
            return {'data': [], 'message': msg_not_allowed.replace('file_name', pdf.filename)}, 400

        if pdf and xml:
            location_pdf = self.storage_service.storage_file_purchases(pdf)
            location_xml = self.storage_service.storage_file_purchases(xml)
            changes = {'pdf_location_file': location_pdf,
                       'xml_location_file': location_xml}
            data = self.purcha_invoice_service.update_locations_files(
                pk, **changes)
            return {
                'data': data,
                'message': 'Los archivos se ha subido',
                'status': 'success'
            }, 200



'''
    def storage_file_purchases(self, file):
        current_date = date_format(str(DataHelper.get_datetime_server()['date']), '%Y-%m-%d')

        folder_name = os.path.join(PATH_FILES_PURCHASES_INVOICE, str(current_date)[0:4],
                                   get_month_name(current_date.month),
                                   str(current_date)[8:10])

        if sys.platform == "win32":
            pathlib.Path(folder_name).mkdir(parents=True, exist_ok=True)
        file.save(os.path.join(folder_name, file.filename))
        return '{0}\{1}'.format(folder_name.replace('files\\', ''), file.filename)
'''