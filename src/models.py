from sqlalchemy import Column, Integer, String
from src.extensions import db


## MODELOS 

class Prospect(db.Model):
    __tablename__ = 'cat_prospectos'
    __table_args__ = {'implicit_returning': False}
    id_prospecto = Column(Integer, primary_key=True)
    nombre = Column(String(50))
    primer_apellido = Column(String(50))
    segundo_apellido = Column(String(50), nullable=True)
    calle = Column(String(50))
    numero_casa = Column(String(50))
    colonia = Column(String(50))
    codigo_postal = Column(Integer)
    telefono = Column(String(30))
    rfc = Column(String(13))
    id_estado = Column(Integer)
    tipo_documento = Column(String(30))
    numero_documento = Column(String(30))

    def __init__(self, nombre, primer_apellido, calle, numero_casa,
                 colonia, codigo_postal, telefono, rfc, id_estado=1, segundo_apellido=None,tipo_documento=None, numero_documento=None):
        self.nombre = nombre
        self.primer_apellido = primer_apellido
        self.segundo_apellido = segundo_apellido
        self.calle = calle
        self.numero_casa = numero_casa
        self.colonia = colonia
        self.codigo_postal = codigo_postal
        self.telefono = telefono
        self.rfc = rfc
        self.id_estado = id_estado
        self.tipo_documento = tipo_documento
        self.numero_documento = numero_documento

class Documents(db.Model):
    __tablename__ = 'cat_documentos'
    __table_args__ = {'implicit_returning': False}
    id_documento = Column(Integer, primary_key=True)
    id_prospecto = Column(Integer)
    tipo_documento = Column(String(20))
    numero_documento = Column(String(20))

    def __init__(self, id_prospecto, tipo_documento, numero_documento):
        self.id_prospecto = id_prospecto
        self.tipo_documento = tipo_documento
        self.numero_documento = numero_documento


class StatusProspect(db.Model):
    __tablename__ = 'cat_estados_prospecto'
    __table_args__ = {'implicit_returning': False}
    id_estado = Column(Integer, primary_key=True)
    nombre = Column(String(10))


class CommentsRefused(db.Model):
    __tablename__ = 'observaciones_rechazo'
    __table_args__ = {'implicit_returning': False}
    id_observacion = Column(Integer, primary_key=True)
    id_prospecto = Column(Integer)
    observaciones = Column(String(300))

    def __init__(self, id_prospecto, observaciones):
        self.id_prospecto = id_prospecto
        self.observaciones = observaciones
