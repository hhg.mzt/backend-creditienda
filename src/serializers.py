from marshmallow import (
    fields,
    Schema
)
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from src.models import Prospect


class ProspectSchema(SQLAlchemyAutoSchema):

    class Meta:
        model = Prospect
        ordered  = True

prospect_schema = ProspectSchema()
prospects_schema = ProspectSchema(many=True)


class ProspectStatus(Schema):
    id_prospecto = fields.Integer()
    nombre = fields.String()
    primer_apellido = fields.String()
    segundo_apellido = fields.String()
    calle = fields.String()
    numero_casa = fields.String()
    colonia = fields.String()
    codigo_postal = fields.Integer()
    telefono = fields.String()
    rfc = fields.String()
    status = fields.String()
    observaciones = fields.String()

    class Meta:
        ordered  = True

prospect_status = ProspectStatus()
prospects_status = ProspectStatus(many=True)
