from sqlalchemy import exc, func
from src.models import (
    Prospect,
    StatusProspect,
    CommentsRefused,
    Documents
)
from src.extensions import db
from src.serializers import (
    prospect_schema,
    prospects_schema,
    prospect_status,
    prospects_status
)

class ProspectService:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    def creat_prospect(self, **kwargs):
        try:
            new_prospect = Prospect(**kwargs)
            db.session.add(new_prospect)
            db.session.commit()
            return (True, prospect_schema.dump(new_prospect))
        except exc.SQLAlchemyError as e:
            db.session.rollback()
            return (False, None)

    def fetch(self):
        #prospects = Prospect.query.all()
        prospects = db\
            .session.query(
                Prospect.id_prospecto, Prospect.nombre, Prospect.primer_apellido,
                func.coalesce(Prospect.segundo_apellido, '').label('segundo_apellido'),
                Prospect.telefono, Prospect.rfc, StatusProspect.nombre.label('status'),
                func.iif(
                    Prospect.id_estado == 3,
                    func.coalesce(CommentsRefused.observaciones, ''),
                    ''
                ).label('observaciones')
            ) \
            .select_from(Prospect) \
            .join(StatusProspect, StatusProspect.id_estado == Prospect.id_estado) \
            .outerjoin(CommentsRefused, CommentsRefused.id_prospecto == Prospect.id_prospecto) \
            .all()

        if not prospects:
            return None
        return prospects_status.dump(prospects)

    def retrieve(self, id_prospect):
        prospect = db\
            .session.query(
                Prospect.id_prospecto, Prospect.nombre, Prospect.primer_apellido,
                func.coalesce(Prospect.segundo_apellido, '').label('segundo_apellido'),
                Prospect.telefono, Prospect.rfc, StatusProspect.nombre.label('status'),
                func.coalesce(CommentsRefused.observaciones, '').label('observaciones')
            ) \
            .select_from(Prospect) \
            .join(StatusProspect, StatusProspect.id_estado == Prospect.id_estado) \
            .outerjoin(CommentsRefused, CommentsRefused.id_prospecto == Prospect.id_prospecto) \
            .filter(Prospect.id_prospecto == id_prospect) \
            .first()
        if not prospect:
            return None
        return prospect_status.dump(prospect)

    def set_status(self, id_prospect, **kwargs):
        prospect = Prospect.query.get_or_404(id_prospect)
        values_status = {
            'Enviado': 1,
            'Autorizado': 2,
            'Rechazado': 3
        }
        try:
            if kwargs['autorizar'] == 1:
                # autorizaremos al prospecto
                pass
                setattr(prospect, 'id_estado', 2)
                db.session.commit()
                return True
            elif kwargs['autorizar'] == 0:
                # rechazaremos al prospecto
                setattr(prospect, 'id_estado', 3)
                exists_comments = CommentsRefused.query.filter(CommentsRefused.id_prospecto == id_prospect).first()
                print('Hola')
                print(exists_comments)
                print('Adios')
                if exists_comments:
                    setattr(exists_comments, 'observaciones', kwargs['observaciones'])
                else:
                    new_comments_refused = CommentsRefused(id_prospecto=id_prospect, observaciones=kwargs['observaciones'])
                    db.session.add(new_comments_refused)
                db.session.commit()
                db.session.commit()
                return True
        except exc.SQLAlchemyError as e:
            db.session.rollback()
            return False

    def create_document(self, **kwargs):
        try:
            new_prospect = Documents(**kwargs)
            db.session.add(new_prospect)
            db.session.commit()
            return True
        except exc.SQLAlchemyError as e:
            db.session.rollback()
            return False


        '''
        @classmethod
        def update(cls, ident, dict_changes):
            # este metodo sólo funciona con modelos que cuenta con llave primaria
            try:
                obj = cls.query.get_or_404(ident)
                # asigmanos los atributos
                for key, value in dict_changes.items():
                    setattr(obj, key, value)

                db.session.commit()
                return True
            except exc.SQLAlchemyError as e:
                db.session.rollback()
                sentry.captureException()
                return False
        '''

    



