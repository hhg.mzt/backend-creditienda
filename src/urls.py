from flask import Blueprint
from flask_restful import Api
from src.controllers import (
    ProspectListController,
    ProspectController,
    DocumentListControllers
)

urls = Blueprint('urls', __name__)
api = Api(urls, prefix='/api')

#Registramos los endpoints

api.add_resource(ProspectListController, '/prospect')
api.add_resource(ProspectController, '/prospect/<int:id_prospect>')
api.add_resource(DocumentListControllers, '/document')
